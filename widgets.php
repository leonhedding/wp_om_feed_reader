<?php

/**
 * The Widget
 */
class OMGCWidget extends WP_Widget {
	/** constructor */
	function OMGCWidget() {
		parent::WP_Widget(false, $name = 'OM Short Term Missions');
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance) {
		//get arguments
	    global $VisitorCountry;
	    global $q_config;

		load_plugin_textdomain('om_feed_reader', false, basename( dirname( __FILE__ ) ) . '/languages' );

		extract( $args );

		//set title var
		$title = 'OM Short Term Missions Widget';

		//get values from config
		$title = (isset($instance['title']) ? apply_filters('widget_title', $instance['title']) : $title);
		$limit = isset($instance['limit']) ? $instance['limit'] : 5;
		$shorten = isset($instance['shorten']) ? $instance['shorten'] : 0;
		$hide_description = isset($instance['hide_description']) ? $instance['hide_description'] : 0;

		$feed_url = OM_APP_SHORTTERMS_URL.'?'; 
        
        /* Get the current country ID */
	    $fromCountryID = $VisitorCountry->GetCode();
	    if (!$fromCountryID) $fromCountryID = 'GB';
	    $feed_url .= "fromCountryId=".$fromCountryID;
	
        /* Get the current language ID */
	    $langCode = $q_config['language'];
	    $feed_url .= "&langCode=".$langCode;

	    if(isset($instance['time']) & $instance['time'])
	    	$feed_url .= "&time=".$instance['time'];
		if(isset($instance['countryId']) && $instance['countryId']!="")
			$feed_url .= "&countryId=".$instance['countryId'];
		if(isset($instance['categoryId']) & $instance['categoryId']>0)
			$feed_url .= "&categoryId=".$instance['categoryId'];
		if(isset($instance['text']) & strlen($instance['text'])>0)
			$feed_url .= "&text=".$instance['text'];
		if(isset($instance['pastDays']) & $instance['pastDays'])
			$feed_url .= "&pastDays=".$instance['pastDays'];

		$url = html_entity_decode($feed_url);
		
        $xml = get_caleb_xml_object($url);

		$return = "";
		$return .= "<ul class=\"om-gc-list\">";

		$i = 0;
		foreach($xml->channel->item as $item) {
			if($i == $limit) break;
			$om = $item->children('om',TRUE);

			/* 
			 * Attempt to narrow the results. In theory, doing this here will help caching. 
			 */
			if(isset($instance['families']) & $instance['families'] & $om->familiesWelcome!="t") break;
			if(isset($instance['groups']) & $instance['groups'] & $om->groupsWelcome!="t") break;
			if(isset($instance['married']) & $instance['married'] & $om->marriedWelcome!="t") break;
			if(isset($instance['age']) & $instance['age'])
			{
				if ($om->minAge!='any' & $om->minAge > $instance['age']) break;
				if ($om->maxAge!='any' & $om->maxAge < $instance['age']) break;
			}

			if( $shorten == 1 )
				$item->title = substr($titel,0,30).'...';

			$return .= ' <li><h4><a href="'.$item->link.'" target="_blank" title="'.($item->title).'" target="_blank" class="om-feed-link">'.($item->title).'</a></h4>';

			if($hide_description == 0)
			    $return .= "<span>".$om->description."</span>";	

			$return .= '</li>';

			$i++;
		}
		
		$return .= "</ul><br /><a href=\"".$xml->channel->link."\" class=\"om-gc-feed-url\">".$xml->channel->link."</a>";

        /* Display the widget */

		echo $before_widget;

		if ( $title )
			echo $before_title . $title . $after_title;
		echo $return;
		echo $after_widget;

	}

	/** @see WP_Widget::update */
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['time'] = strip_tags($new_instance['time']);
		$instance['categoryId'] = strip_tags($new_instance['categoryId']);
		$instance['countryId'] = strip_tags($new_instance['countryId']);
		$instance['text'] = strip_tags($new_instance['text']);
		$instance['families'] = strip_tags($new_instance['families']);
		$instance['groups'] = strip_tags($new_instance['groups']);
		$instance['married'] = strip_tags($new_instance['married']);
		$instance['age'] = strip_tags($new_instance['age']);
		$instance['pastDays'] = strip_tags($new_instance['pastDays']);
		$instance['limit'] = (int)($new_instance['limit']);
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['shorten'] = (int)($new_instance['shorten']);
		$instance['hide_description'] = (int)($new_instance['hide_description']);
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance) {
		/*in8n*/
        load_plugin_textdomain('om_feed_reader', false, basename( dirname( __FILE__ ) ) . '/languages' );

        /* load values */
        $title = (isset($instance['title']) ? esc_attr($instance['title']) : '');
        $limit = (isset($instance['limit']) ? esc_attr($instance['limit']) : 5);
        $shorten = (isset($instance['shorten']) ? esc_attr($instance['shorten']) : 0);
        $families = (isset($instance['families']) ? esc_attr($instance['families']) : 0);
        $groups = (isset($instance['groups']) ? esc_attr($instance['groups']) : 0);
        $married = (isset($instance['married']) ? esc_attr($instance['married']) : 0);
        $time = (isset($instance['time']) ? esc_attr($instance['time']) : '');
        $countryId = (isset($instance['countryId']) ? esc_attr($instance['countryId']) : '');
        $categoryId = (isset($instance['categoryId']) ? esc_attr($instance['categoryId']) : '');
        $text = (isset($instance['text']) ? esc_attr($instance['text']) : '');
        $hide_description = (isset($instance['hide_description']) ? esc_attr($instance['hide_description']) : 0);
        ?>
    <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:','om_feed_reader'); ?>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label>

            <br />
            <label for="<?php echo $this->get_field_id('time'); ?>"><?php _e('Time:','om_feed_reader'); ?>
            <input class="widefat" id="<?php echo $this->get_field_id('time'); ?>" name="<?php echo $this->get_field_name('time'); ?>" type="text" value="<?php echo $time; ?>" /></label>

            <br />
            <label for="<?php echo $this->get_field_id('countryId'); ?>"><?php _e('Country Id:','om_feed_reader'); ?>
            <input class="widefat" id="<?php echo $this->get_field_id('countryId'); ?>" name="<?php echo $this->get_field_name('countryId'); ?>" type="text" value="<?php echo $countryId; ?>" /></label>

            <br />
            <label for="<?php echo $this->get_field_id('categoryId'); ?>"><?php _e('categoryId:','om_feed_reader'); ?>
            <input class="widefat" id="<?php echo $this->get_field_id('categoryId'); ?>" name="<?php echo $this->get_field_name('categoryId'); ?>" type="text" value="<?php echo $categoryId; ?>" /></label>
            
            <br />
            <label for="<?php echo $this->get_field_id('text'); ?>"><?php _e('Text Search:','om_feed_reader'); ?>
            <input class="widefat" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>" type="text" value="<?php echo $text; ?>" /></label>
            
            <br />
            <label for="<?php echo $this->get_field_id('families'); ?>"><?php _e('For families:','om_feed_reader'); ?> &nbsp;
            <input type="checkbox" id="<?php echo $this->get_field_id('families'); ?>" value="1" name="<?php echo $this->get_field_name('families'); ?>" <?php if($families == 1) echo 'checked="checked"'; ?> /></label>
            
            <br />
            <label for="<?php echo $this->get_field_id('groups'); ?>"><?php _e('For groups:','om_feed_reader'); ?> &nbsp;
            <input type="checkbox" id="<?php echo $this->get_field_id('groups'); ?>" value="1" name="<?php echo $this->get_field_name('groups'); ?>" <?php if($groups == 1) echo 'checked="checked"'; ?> /></label>
            
            <br />
            <label for="<?php echo $this->get_field_id('married'); ?>"><?php _e('For married:','om_feed_reader'); ?> &nbsp;
            <input type="checkbox" id="<?php echo $this->get_field_id('married'); ?>" value="1" name="<?php echo $this->get_field_name('married'); ?>" <?php if($married == 1) echo 'checked="checked"'; ?> /></label>
            
            <br />
            <label for="<?php echo $this->get_field_id('limit'); ?>"><?php _e('No# messages:','om_feed_reader'); ?>
            <input class="widefat" id="<?php echo $this->get_field_id('limit'); ?>" name="<?php echo $this->get_field_name('limit'); ?>" type="text" value="<?php echo $limit; ?>" /></label>

            <br />
            <label for="<?php echo $this->get_field_id('shorten'); ?>"><?php _e('Shorten link:','om_feed_reader'); ?> &nbsp;
            <input type="checkbox" id="<?php echo $this->get_field_id('shorten'); ?>" value="1" name="<?php echo $this->get_field_name('shorten'); ?>" <?php if($shorten == 1) echo 'checked="checked"'; ?> /></label>

            <br />
            <label for="<?php echo $this->get_field_id('hide_description'); ?>"><?php _e('Hide description:','om_feed_reader'); ?> &nbsp;
            <input type="checkbox" id="<?php echo $this->get_field_id('hide_description'); ?>" value="1" name="<?php echo $this->get_field_name('hide_description'); ?>" <?php if($hide_description == 1) echo 'checked="checked"'; ?> /></label>
        </p>
        <?php 
    }

}

/**
 * Jobs Widget
 */
class OMJobsWidget extends WP_Widget {
    var $title = 'OM Jobs Widget';
    var $limit = 5;
    var $shorten = 0;
    var $hide_description = 0;
    var $feed_file = OM_APP_JOBS_URL; 
	
    /** constructor */
	function OMJobsWidget() {
		parent::WP_Widget(false, $name = 'OM Jobs');
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance) {

		load_plugin_textdomain('om_feed_reader', false, basename( dirname( __FILE__ ) ) . '/languages' );

		extract( $args );

		//get defaults 
        $title = (isset($instance['title']) ? apply_filters('widget_title', $instance['title']) : $this->title);
        $limit = (isset($instance['limit']) ? $instance['limit'] : $this->limit);
		$shorten = (isset($instance['shorten']) ? $instance['shorten'] : $this->shorten);
		$hide_description = (isset($instance['hide_description']) ? $instance['hide_description'] : $this->hide_description);

        /* Construct URL */
		$feed_url = $this->base_url.$this->feed_file.'?';
		if(isset($instance['countryId']) && $instance['countryId']!="")
			$feed_url .= "&country=".$instance['countryId'];
		if(isset($instance['categoryId']) & $instance['categoryId']>0)
			$feed_url .= "&categoryId=".$instance['categoryId'];
		if(isset($instance['text']) & strlen($instance['text'])>0)
			$feed_url .= "&freeText=".$instance['text'];

		$url = html_entity_decode($feed_url);
	        
        $xml = get_caleb_xml_object($url);
		
		$return = "";
		$return .= "<ul class=\"om-jobs-list\">";

		$i = 0;
		foreach($xml->channel->item as $item) {
			if($i == $limit) break;
			$om = $item->children('om',TRUE);

			if( $shorten == 1 )
				$item->title = substr($titel,0,30).'...';

			$return .= ' <li><h4><a href="'.$item->link.'" target="_blank" title="'.($item->title).'" target="_blank" class="om-feed-link">'.($item->title).'</a></h4>';

			if($hide_description == 0)
			$return .= "<span>".$om->description."</span>";	

			$return .= '</li>';

			$i++;
		}
		
		$return .= "</ul><br /><a href=\"".$xml->channel->link."\" class=\"om-jobs-feed-url\">".$xml->channel->link."</a>";

        /* display widget */
		echo $before_widget;

		if ( $title )
			echo $before_title . $title . $after_title;
		echo $return;
		echo $after_widget;

	}

	/** @see WP_Widget::update */
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['categoryId'] = strip_tags($new_instance['categoryId']);
		$instance['countryId'] = strip_tags($new_instance['countryId']);
		$instance['text'] = strip_tags($new_instance['text']);
		$instance['limit'] = (int)($new_instance['limit']);
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['shorten'] = (int)($new_instance['shorten']);
		$instance['hide_description'] = (int)($new_instance['hide_description']);
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance) {
        $title = (isset($instance['title']) ? esc_attr($instance['title']) : '');
        $limit = (isset($instance['limit']) ? esc_attr($instance['limit']) : 5);
        $shorten = (isset($instance['shorten']) ? esc_attr($instance['shorten']) : 0);
        $countryId = (isset($instance['countryId']) ? esc_attr($instance['countryId']) : '');
        $categoryId = (isset($instance['categoryId']) ? esc_attr($instance['categoryId']) : '');
        $text = (isset($instance['text']) ? esc_attr($instance['text']) : '');
        $hide_description = (isset($instance['hide_description']) ? esc_attr($instance['hide_description']) : 0);
		?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:','om_feed_reader'); ?>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label>

            <br />
            <label for="<?php echo $this->get_field_id('countryId'); ?>"><?php _e('Country Id:','om_feed_reader'); ?>
            <input class="widefat" id="<?php echo $this->get_field_id('countryId'); ?>" name="<?php echo $this->get_field_name('countryId'); ?>" type="text" value="<?php echo $countryId; ?>" /></label>

            <br />
            <label for="<?php echo $this->get_field_id('categoryId'); ?>"><?php _e('categoryId:','om_feed_reader'); ?>
            <input class="widefat" id="<?php echo $this->get_field_id('categoryId'); ?>" name="<?php echo $this->get_field_name('categoryId'); ?>" type="text" value="<?php echo $categoryId; ?>" /></label>
            
            <br />
            <label for="<?php echo $this->get_field_id('text'); ?>"><?php _e('Text Search:','om_feed_reader'); ?>
            <input class="widefat" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>" type="text" value="<?php echo $text; ?>" /></label>
            
            <br />
            <label for="<?php echo $this->get_field_id('limit'); ?>"><?php _e('No# Jobs:','om_feed_reader'); ?>
            <input class="widefat" id="<?php echo $this->get_field_id('limit'); ?>" name="<?php echo $this->get_field_name('limit'); ?>" type="text" value="<?php echo $limit; ?>" /></label>

            <br />
            <label for="<?php echo $this->get_field_id('shorten'); ?>"><?php _e('Shorten link:','om_feed_reader'); ?> &nbsp;
            <input type="checkbox" id="<?php echo $this->get_field_id('shorten'); ?>" value="1" name="<?php echo $this->get_field_name('shorten'); ?>" <?php if($shorten == 1) echo 'checked="checked"'; ?> /></label>

            <br />
            <label for="<?php echo $this->get_field_id('hide_description'); ?>"><?php _e('Hide description:','om_feed_reader'); ?> &nbsp;
            <input type="checkbox" id="<?php echo $this->get_field_id('hide_description'); ?>" value="1" name="<?php echo $this->get_field_name('hide_description'); ?>" <?php if($hide_description == 1) echo 'checked="checked"'; ?> /></label>
        </p>
        <?php 
    }

}

?>
