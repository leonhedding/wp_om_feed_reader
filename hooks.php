<?php

add_action('widgets_init', create_function('', 'return register_widget("OMGCWidget");'));
add_action('widgets_init', create_function('', 'return register_widget("OMJobsWidget");'));
add_shortcode( 'om-gc-feed-reader', 'om_gc_feed_shorttag'); //leave old for backwards compatibility.
add_shortcode( 'om_gc_feed_reader', 'om_gc_feed_shorttag');
add_shortcode( 'om-jobs-feed-reader', 'om_jobs_feed_shorttag'); //leave old for backwards compatibility.
add_shortcode( 'om_jobs_feed_reader', 'om_jobs_feed_shorttag');
add_shortcode( 'om_work_detail', 'om_work_detail_shorttag');
add_shortcode('om-contact-display','om_contact_display_shorttag'); //leave old for backwards compatibility.
add_shortcode('om_contact_display','om_contact_display_shorttag'); 
add_shortcode('om_single_resource','om_single_resource_shorttag');
add_shortcode('om-resource','om_single_resource_shorttag');
add_shortcode('om-resources','om_resources_shorttag'); //leave old for backwards compatibility.
add_shortcode('om_resources','om_resources_shorttag');

if (class_exists('WPCF7_ContactForm')) {
    add_action("wpcf7_before_send_mail", "wpcf7_om_recipient");
}
?>
