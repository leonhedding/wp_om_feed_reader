<?php

function set_om_feed_defaults()
{
    $options = array(
        'cache_timeout'            => '12',
    );
    update_option('om-reader_options', $options);
    return;
}

register_activation_hook(__FILE__, 'set_om_feed_defaults');
register_activation_hook( __FILE__, 'om_activation' );
/**
 * On activation, set a time, frequency and name of an action hook to be scheduled.
 */
function om_activation()
{
    wp_schedule_event( time(), 'daily', 'om_clean_cache' );
}

add_action( 'om_clean_cache', 'om_flush_cache_daily' );
/**
 * On the scheduled action hook, run the function.
 */
function om_flush_cache_daily()
{
    global $wpdb;

    $sql = "
        delete from {$wpdb->options}
        where option_name like '_transient_caleb_rss_%' or option_name like '_transient_timeout_caleb_rss_%'
        ";
    $wpdb->query($sql);
}

register_deactivation_hook( __FILE__, 'om_deactivation' );
/**
 * On deactivation, remove all functions from the scheduled action hook.
 */
function om_deactivation()
{
    wp_clear_scheduled_hook( 'om_clean_cache' );
    delete_option('om-reader_options');
}
?>
