<?php
$menu_items = array();
$items = "";

    foreach($xml->channel->item as $item) {
        $i++;
        $om = $item->children('om',true);
        $title = $om->title;

        if ($atts['categoryid2']) {
            $feed_categories = explode(',',$om->categoryIds);
            $want_categories = explode(',',$atts['categoryid2']);

            $a_category=false;
            foreach ($want_categories as $want_category) if (in_array($want_category,$feed_categories)) $a_category=true;
            if (!$a_category) continue;
        }

        if ((String)$om->country == $previouscountry) {
            $countryHeading = "";
        } else {
            $menu_items[] = (String)$om->country;
        }

        $link = $item->link;
        ob_start();
        include('templates/default/item.html.php');
        $items .= ob_get_contents();
        ob_end_clean();
        $previouscountry = $om->country;
    }
ob_start();
include_once('templates/default/page.html.php');
$return = ob_get_contents();
ob_end_clean();
$return = $menu.$return;
