<?php
/*
Plugin Name: OM Caleb RSS Feed Reader
Description: Plugin to view an rss feed from Caleb on your page or post. 
	[om-gc-feed-reader time=11-2006 countryID=UG categoryID=25 text="Surfing" length=10 
	start=10 families=1/0 groups=1/0 married=1/0 age=16 pastDays=-30 plain=0/1]
or	[om-jobs-feed-reader category=1 country=IE region=17 freeText="Programmer" length=10 plain=0/1]
	Simply use the shortcode and the RSS feed will be shown in HTML to your visitor
	All options are optional, so simply [om-jobs-feed-reader] will work.
Version: 0.0.5
Text Domain: om_feed_reader
Author: OMNIvision
Author URI: http://www.omnivision.org.uk
License: GPL2
*/

// TODO split out the various bits of this file into appropriate include files
// TODO probably should rename gc to shortterm where possible

define('OM_APP_URL','http://app.om.org/rss/');
define('OM_APP_RESOURCE_URL',OM_APP_URL.'resource.rss');
define('OM_APP_RESOURCES_URL',OM_APP_URL.'resources.rss');
define('OM_APP_SHORTTERMS_URL',OM_APP_URL.'challenge.rss');
define('OM_APP_SHORTTERM_URL',OM_APP_URL.'optioninfo.rss');
define('OM_APP_JOBS_URL',OM_APP_URL.'opportunities.rss');
define('OM_APP_JOB_URL',OM_APP_URL.'opportunity.rss');
define('OM_APP_CONTACTS_URL',OM_APP_URL.'contacts.rss');

require_once(dirname(__FILE__)."/functions.php");
require_once(dirname(__FILE__)."/shortcodes.php");
require_once(dirname(__FILE__)."/widgets.php");

if( is_admin() ) {
    require_once(dirname(__FILE__)."/options.php");
    require_once(dirname(__FILE__)."/setup.php");
}
// add this at the end
require_once(dirname(__FILE__)."/hooks.php");
?>
