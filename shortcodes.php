<?php

/*
* short tag functions for short term and jobs 
* 
* enables:
* [om-gc-feed time=x countryId=ABC categoryId=99 text=ABC length=99 start=date families=0/1 groups=0/1 married=0/1 age=99 pastDays=99 plain=0/1]
* [om-jobs-feed country=ABC text=ABC length=99 plain=0/1]
* 
*/
function om_gc_feed_shorttag($atts) {
    global $VisitorCountry;
    global $q_config;
    $options = wp_parse_args(get_option('om-reader_options'));
    
    $feed_url = OM_APP_SHORTTERMS_URL.'?';

    // TODO: fromCountryID can be specified more than once so we need to account for this. So that OM Africa can run a site for all of the African fields.
    if (isset($VisitorCountry)) $fromCountryID = $VisitorCountry->GetCode();
    if (!$fromCountryID) $fromCountryID = $options['from_country_id'];
    $feed_url .= "fromCountryId=".$fromCountryID;

    $langCode = $q_config['language'];
    $feed_url .= "&langCode=".$langCode;

    $atts = shortcode_atts(array(
    'time'         => null,
    'countryid'    => null,
    'categoryid'   => null,
    'text'         => null,
    'length'       => '10',
    'start'        => null,
    'families'     => null,
    'groups'       => null,
    'married'      => null,
    'age'          => null,
    'pastdays'     => null,
    'groupby'      => 'country',
    'plain'        => '1',
    ), $atts);

    foreach ($atts as $key => $val)
    {
        if ($key=='countryid') $key='countryId';
        if ($key=='categoryid') $key='categoryId';
        if (isset($val)) $feed_url .= "&".$key."=".$val;
    }

    if ($return = om_rss_feed_reader('gc',$feed_url,$limit,$atts)) return $return;
    else { // If no results get the British Results
        $feed_url = str_replace("fromCountryId=".$fromCountryID, "fromCountryId=GB", $feed_url);
        $return = om_rss_feed_reader('gc',$feed_url,$limit,$atts);
        if ($return) return $return;
        else {
            return __('Sorry, no results','om_feed_reader');
        }
    }
}

function om_jobs_feed_shorttag($atts) {
    $feed_url = OM_APP_JOBS_URL.'?';

    $atts = shortcode_atts(array(
    'country'     => null,
    'category'    => null,
    'region'      => null,
    'freeText'    => null,
    'length'      => '2',
    'groupby'     => 'country',
    'plain'       => '1',
    ), $atts);

    foreach ($atts as $key=>$val)
    {
        if ($val) $feed_url .= "&".$key."=".$val;
    }

    $return = om_rss_feed_reader('jobs',$feed_url,$limit,$atts);
    if ($return) return $return;
    else {
        return __('Sorry, no results','om_feed_reader');
    }
}

function om_work_detail_shorttag($atts) {
    $id = urlencode($_GET['id']);
    $fromcountryid = urlencode($_GET['fromCountryId']);
    $langcode = urlencode($_GET['langCode']);
    if (preg_match("/s/i", $id)) {
        $type = '0';    
        $url = OM_APP_JOB_URL."?jobId=".$id;
    }
    else {
        $type = '1';    
        $url = OM_APP_SHORTTERM_URL."?optionId=".$id."&fromCountryId=".$fromcountryid."&=langCode=".$langcode;
    }
    $url = html_entity_decode($url);
    $xml = get_caleb_xml_object($url);

    $item = $xml->channel->item;
    $om = $item->children('om',true);

    switch ($type) {
        case 0:
            $pleasenote = 'All jobs/opportunities on this site are unsalaried. Most people joining OM have to raise financial support to cover their living expenses, usually through gifts from home churches and other supporters.';

            $display .= ""
                .'<h2>'.$om->title.' <span class="country">('.$om->country.')</span></h2>'
                .'<blockquote>'.$om->description.'</blockquote></br>'
                .'<div class="info-line"><label>Start Date</label><p class="info-value">'.$om->startDate.'</p></div>'
                .'<div class="info-line"><label>Commitment Length</label><p class="info-value">'.$om->commitmentLength.'</p></div>'
                .'<div class="info-line"><label>Requirements</label><p class="info-value">'.$om->requirements.'</p></div>'
                .'<div class="info-line"><label>Please Note</label><p class="info-value">'.$pleasenote.'</p></div>';
            break;
        case 1:
            $title = $om->name;
            $description = $om->description;
            $country = $om->country;
            $startDate = $om->startDate;
            $endDate = $om->endDate;
            $cost = $om->cost;
            $currency = $om->currency;
            $country = $om->country;
            $deadline = $om->applicationDeadline;
            $generalNotes = $om->generalNotes;
            $details = $om->ministryDetails;
            $profile = $om->participantProfile;
            $notes = $om->notes;
            $accommodation = $om->accommodation;
            $food = $om->food;
            $travel = $om->travel;
            $health = $om->health;
            $visa = $om->visa;
            $minAge = $om->minAge;
            $maxAge = $om->maxAge;
            $married = $om->marriedCouples;
            $family = $om->families;
            $group = $om->groups;

            $display .= ""
                .'<h2>'.$title.' <span class="country">('.$country.')</span></h2>'
                .'<blockquote>'.$description.'<br><b>'.$generalNotes.'</b></blockquote></br>'
                .'<div class="info-=line">'
                .'<img src="'.plugins_url("layouts/shortterms/templates/default/images/single-s-y.png",__FILE__).'"'
                    .'title="'.__("Single people welcome","om_feed_reader").'">'
                .'<img src="'.plugins_url('layouts/shortterms/templates/default/images/married-s-'.($married == 'y' ? 'y':'n').'.png',__FILE__).'"'
                    .'title="'.(($married == 'y')?__("Married couples welcome",'om_feed_reader')
                        :__("Sorry, no married couples",'om_feed_reader')).'">'
                .'<img src="'.plugins_url('layouts/shortterms/templates/default/images/family-s-'.($family == 'y' ? 'y':'n').'.png',__FILE__).'"'
                    .'title="'.(($family == 'y')?__("Families welcome",'om_feed_reader')
                        :__("Sorry, no families",'om_feed_reader')).'">'
                .'<img src="'.plugins_url('layouts/shortterms/templates/default/images/group-s-'.($group == 'y' ? 'y':'n').'.png',__FILE__).'"'
                    .'title="'.(($group == 'y')?__("Groups welcome",'om_feed_reader')
                        :__("Sorry, no groups",'om_feed_reader')).'">'
                .'</div>'
                .'<div class="info-line"><label>Dates</label><p class="info-value">'.$startDate.' - '.$endDate.'</p></div>'
                .'<div class="info-line"><label>Apply By</label><p class="info-value">'.$deadline.'</p></div>'
                .'<div class="info-line"><label>Cost</label><p class="info-value">'.$cost.' <small>['.$currency.']</small></p></div>'
                .'<div class="info-line"><label>Ages</label><p class="info-value">'.$minAge.' - '.$maxAge.'</p></div>'
                .'<div class="info-line"><label>Ministry Details</label><p class="info-value">'.$details.'</p></div>'
                .'<div class="info-line"><label>Participant Profile</label><p class="info-value">'.$profile.'</p></div>'
                .'<div class="info-line"><label>Accommodation</label><p class="info-value">'.$accommodation.'</p></div>'
                .'<div class="info-line"><label>Food</label><p class="info-value">'.$food.'</p></div>'
                .'<div class="info-line"><label>Travel</label><p class="info-value">'.$travel.'</p></div>'
                .'<div class="info-line"><label>Health</label><p class="info-value">'.$health.'</p></div>'
                .'<div class="info-line"><label>Visa</label><p class="info-value">'.$visa.'</p></div>'
                .'<div class="info-line"><label>Notes</label><p class="info-value">'.$notes.'</p></div>'
                ;
            break;
    }
    return $display;

}

/* 
* Short tag function for displaying the local OM office's contact details
*/
function om_contact_display_shorttag($atts)
{
    global $om_local_field_gc_contact;
    global $om_local_field_contact;

    extract(shortcode_atts(array(
    'type'    => 'gc',
    'country'    => 'none',
    ), $atts));

    if (!om_contact_info($country)) return __("Sorry, no contact information for your local field","om_feed_reader");

    /* return the requested type of contact - fallback to the other type or null */ 
    switch ($type) {
        case 'general':
            if (isset($om_local_field_contact)) $contact = $om_local_field_contact; 
            elseif (isset($om_local_field_gc_contact)) $contact = $om_local_field_gc_contact;
            break;
        case 'gc': default:
            if (isset($om_local_field_gc_contact)) $contact = $om_local_field_gc_contact;
            elseif (isset($om_local_field_contact)) $contact = $om_local_field_contact; 
            break;
    }

    $scriptopen = '<SCRIPT LANGUAGE="JavaScript" type="text/javascript"> ';
    $scriptclose = ''
			." </SCRIPT>"
			."<NOSCRIPT>"
			."<em>".__("Email address protected by JavaScript. Activate javascript to see the email.","om_feed_reader")."</em>"
			."</NOSCRIPT>\n";

    $emailregex = '(([a-zA-Z0-9\.]+)\@([a-zA-Z0-9\-]+\..+))';
    $emailreplace = $scriptopen."gen_mail_to_link('$1','$2','Website Enquiry');".$scriptclose;
    $webregex = "%((https?://)|(www\.))(([a-z0-9\-]+\.?)+)/?(([a-zA-Z0-9\-]+/?)*)%i"; 
    $webreplace = '<a href="http://$1$4/$6">$1$4/$6</a>';

    $contact['email'] = preg_replace($emailregex,$emailreplace,$contact['email']);
    $contact['address'] = nl2br(preg_replace(array($webregex,$emailregex),array($webreplace,$emailreplace),$contact['address']));

    ob_start();
    include('layouts/contact/templates/default/item.html.php');
    $item = ob_get_contents();
    ob_end_clean();
    ob_start();
    include('layouts/contact/templates/default/page.html.php');
    $return = ob_get_contents();
    ob_end_clean();

    return $return;
}

/* short tag function for displaying a single resource */
function om_single_resource_shorttag($atts)
{
    $resource_id = urlencode($_GET['resourceID']);
    $url = OM_APP_RESOURCE_URL."?id=".$resource_id;
    $url = html_entity_decode($url);
    $xml = get_caleb_xml_object($url);

    $item = $xml->channel->item;
    $om = $item->children('om',true);

    /* display format varies with media type */
    switch ($om->mediaTypeId) {
        case 0: /* Image */
            $display .= ""
            ."<a href='".$om->mediaUrl."'><img src='".$om->mediaUrl."'></a>"
            ."<h2>".$item->title."</h2>"
            ."<p>".$item->description."</p>"
            ."<span>Photo by ".$om->authorName." - ".$om->creditDescription."</span>"
            ."<div style='clear:both;'></div>"
            ."";
            break;
        case 4: default: /* Article, or other */
            $display .= ""
            ."<h2>".$item->title."</h2>"
            ."<a data-rokbox rel='rokbox' href='http://www.om.org/img/m".$om->attachedPhotoId
            .".jpg' rel='rokbox'><img src='http://www.om.org/img/r".$om->attachedPhotoId.".jpg' "
            ."style='float:right; max-width:50% !important; min-width:300px;'></a>"
            ."<p>".$item->pubDate."</p>"
            .$om->full
            ."<div style='clear:both;'></div>";
            break;
    }

    return $display;
}

function om_resources_shorttag($atts)
{
    $feed_url = OM_APP_RESOURCES_URL."?";

    $atts = shortcode_atts(array(
    'mediatypeid'      => null,
    'region'           => null,
    'countryid'        => null,
    'categoryid'       => null,
    'text'             => null,
    'length'           => '10',
    'start'            => null,
    'sort0'            => null,
    'sort1'            => null,
    'sort2'            => null,
    'field'            => null,
    'pin'              => null,
    'includepublic'    => null,
    'plain'            => '1',
    ), $atts);

    foreach ($atts as $key => $val)
    {
        if ($key=='mediatypeid') $key='mediaTypeId';
        if ($key=='countryid') $key='countryId';
        if ($key=='categoryid') $key='categoryId';
        if ($key=='includepublic') $key='includePublic';
        if (isset($val)) $feed_url .= "&".$key."=".$val;
    }

    $return = om_rss_feed_reader('resources',$feed_url,$limit,$atts);
    if ($return) return $return;
    else {
        return __('Sorry, no results','om_feed_reader');
    }
}

?>
