<?php
add_action('init','om_feed_reader_buttons');
function om_feed_reader_buttons() {
	add_filter("mce_external_plugins","om_feed_reader_add_buttons");
	add_filter("mce_buttons","om_feed_reader_register_buttons");
}
function om_feed_reader_add_buttons($plugin_array) {
	$plugin_array['om_feed_reader'] = plugins_url('mce.js',__FILE__);
	return $plugin_array;
}
function om_feed_reader_register_buttons($buttons) {
	array_push($buttons,'shortterm','jobs'); 
	return $buttons;
}

// clear the transient cache
function flush_caleb_cache()
{
    check_admin_referer('om-rss-action', 'om-rss-nonce');

    if ( isset ( $_POST[flush_rss_cache] ) )
    {
        $cache = 'cleared';
    }

    global $wpdb;

    $sql = "
        delete from {$wpdb->options}
        where option_name like '_transient_caleb_rss_%' or option_name like '_transient_timeout_caleb_rss_%'
        ";
    $wpdb->query($sql);


    $url = add_query_arg( 'cache', $cache, urldecode( admin_url( 'options-general.php?page=om-rss-feed-reader') ) );
    wp_safe_redirect( $url );
    exit;
}

function get_caleb_feed($url)
{
    $data = false;
    if ($url) {
        $response = wp_remote_get($url, array('timeout' => 15));
        if (!is_wp_error($response)) {
            $data = wp_remote_retrieve_body($response);
        }
    }
    return $data ? $data : false;
}

// check for cached text from a url, otherwise fetch it
// then convert it to an xml object
function get_caleb_xml_object($url)
{
    $key = md5($url);
    if (false === ($xml = get_transient('caleb_rss_'.$key))) {
        $xml = get_caleb_feed($url);
        if ($xml) {
            $options = wp_parse_args(get_option('om-reader_options'));
            set_transient('caleb_rss_'.$key, $xml, $options[cache_timeout] * HOUR_IN_SECONDS); // 12 hours
        }
    }
    return $xml ? @simplexml_load_string($xml) : false;
}

function OmFeedfirstWords($string,$words=100){
    $wo = explode(" ",$string);
    $c=0;
    $return='';
    foreach ($wo as $piece){
        $c++;
        if($words>$c)
            $return .= " ".$piece;
    }
    return $return;
}

function om_rss_feed_reader($style,$url,$limit=50,$atts=array()){

    load_plugin_textdomain('om_feed_reader', false, basename( dirname( __FILE__ ) ) . '/' );

    $url = html_entity_decode($url);

    $xml = get_caleb_xml_object($url);

    $i=0;
    $lastcountry = "";
    if (!count($xml->channel->item))
        return false;

    switch($style) {
        case "gc":
            include('layouts/shortterms/index.php');
            return $return;
        case "jobs":
            include('layouts/jobs/index.php');
            return $return;
        case "resources":
            include('layouts/resources/index.php');
            return $return;
        default: break;
    }

    return $return;
}

function om_contact_info($country)
{
    global $om_local_field_gc_contact;
    global $om_local_field_contact;
    global $VisitorCountry; // requires visitor country plugin
    $options = wp_parse_args(get_option('om-reader_options'));
    if ($country === 'none') {
        if (isset($VisitorCountry)) $fromCountryID = $VisitorCountry->GetCode();
        if (!$fromCountryID)
            $fromCountryID = $options['from_country_id'];
    }
    else
        $fromCountryID = $country;

    /* Get the rss file. Yes, you just have to get ALL the contacts */
    $xml = get_caleb_xml_object(OM_APP_CONTACTS_URL);

    /* Do nothing if there's no items */
    if (!count($xml->channel->item))
        return false;

    foreach($xml->channel->item as $item)
    {
        $om = $item->children('om',true);
        if ($om->countryId == $fromCountryID)
        {
            $field = array(
                'unitName' => $om->unitName,
                'tel' => $om->tel,
                'address' => $om->address,
                'email' => $om->email
            );

            /* Classify the contact as GC or General. */
            if ($om->type=="GC")
            {
                $om_local_field_gc_contact = $field;
            }
            else
            {
                $om_local_field_contact = $field;
            }
            unset($field);
        }
    }
    if (!isset($om_local_field_gc_contact['unitName']) && !isset($om_local_field_contact['unitName'])) return false; 
    else return true;
}

/**
* function to add the general or short term em ail addresa as the recipient to CF7 forms
*/
function wpcf7_om_recipient (&$wpcf7) {
    global $om_local_field_gc_contact;
    global $om_local_field_contact;

    // query the additional settings section of the form and if it has
    // contact_info: shortterm
    // OR
    // contact_info: general
    // we then change the appropriate recipient for the form. 
    $contact_type = $wpcf7->additional_setting('contact_info', false);
    if (is_array($contact_type) && count($contact_type) > 0) {
        om_contact_info('none');
        
        switch ($contact_type) {
            case "shortterm":
                // this makes certain that the email will still get through no matter what
                $wpcf7->mail['recipient'] = isset($om_local_field_gc_contact) ? $om_local_field_gc_contact['email'] : $om_local_field_contact['email'];
                break;
            case 'general': default:
                // this makes certain that the email will still get through no matter what
                $wpcf7->mail['recipient'] = isset($om_local_field_contact['email']) ? $om_local_field_contact['email'] : $om_local_field_gc_contact['email'];
                break;
        }
    }
    $wpcf7_data->mail['body'] .= $wpcf7->additional_setting('contact_info');
    return $wpcf7;
}
?>
